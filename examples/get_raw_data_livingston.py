"""
Gets raw L1:CAL-DELTAL_EXTERNAL_DQ data time series.
Use Livingston host_server:port_number = nds.ligo-la.caltech.edu:31200

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu

channels = ["L1:CAL-DELTAL_EXTERNAL_DQ", "L1:LSC-REFL_SERVO_ERR_OUT_DQ"]
gps_start = 1256771562
gps_stop = 1256771712

host_server = "nds.ligo.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

dataDict = nu.acquire_data(
    channels,
    gps_start,
    gps_stop,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

cal_deltal_external_data = dataDict["L1:CAL-DELTAL_EXTERNAL_DQ"][
    "data"
]  # this is where the data is stored

# Plot raw time series data
import matplotlib.pyplot as plt

nu.plot_raw_data(dataDict, seconds=150, downsample=2**10)
plt.show()
