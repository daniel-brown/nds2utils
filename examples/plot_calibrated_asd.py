"""
Plots the calibrated DARM ASD.

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu

channels = ["H1:CAL-DELTAL_EXTERNAL_DQ", "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ"]
gps_start = 1256771562
gps_stop = 1256771712
binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

host_server = "nds.ligo.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

dataDict = nu.get_psds(
    channels,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

# Make the DARM calibration using zpk
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
units = "m"
dataDict = nu.calibrate_chan(
    dataDict,
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,
)

# Define some plot parameters
plot_chans = [
    "H1:CAL-DELTAL_EXTERNAL_DQ"
]  # only plot DARM, recall that this data dict contains ISS info as well and plot_ASDs will plot it
xlims = [7, 7000]  # set the x-axis limits for the plot
ylims = [1e-20, 1e-15]  # set the y-axis limits for the plot

import matplotlib.pyplot as plt

nu.plot_asds(
    dataDict,
    plot_chans=plot_chans,
    title="Calibrated and Logbinned DARM spectra",
    logbin=True,
    xlims=xlims,
    ylims=ylims,
    units=units,
)
plt.show()
