"""
Plots the raw ASDs at two different times for comparison.

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu

channels = ["H1:CAL-DELTAL_EXTERNAL_DQ", "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ"]
gps_start_bad = 1256771396
gps_stop_bad = 1256771546
gps_start_good = 1256771562
gps_stop_good = 1256771712

binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

hostname = "nds.ligo.caltech.edu"
port = 31200
allow_data_on_tape = "True"

data_dict_bad = nu.get_psds(
    channels,
    gps_start_bad,
    gps_stop_bad,
    binwidth,
    overlap,
    host_server=hostname,
    port_number=port,
    allow_data_on_tape=allow_data_on_tape,
)
data_dict_good = nu.get_psds(
    channels,
    gps_start_good,
    gps_stop_good,
    binwidth,
    overlap,
    host_server=hostname,
    port_number=port,
    allow_data_on_tape=allow_data_on_tape,
)

import matplotlib.pyplot as plt

nu.compare_asds(
    data_dict_bad,
    data_dict_good,
    label1="Bad",
    label2="Good",
    title="Uncalibrated and Logbinned DARM and ISS spectra",
    logbin=True,
)
plt.show()
