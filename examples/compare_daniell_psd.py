"""take_daniell_psd.py

Take a PSD using the Daniell method instead of Welch's method.
Basically it is a frequency-domain averaging, not time-domain averaging method.
Reference:
http://www.alekslabuda.com/sites/default/files/publications/[2016-03]%20Daniell%20method%20for%20PSD%20estimation%20in%20AFM.pdf

Craig Cahillane
May 2, 2023
"""

import os
import numpy as np
import copy
import matplotlib as mpl
import matplotlib.pyplot as plt
import nds2utils as nu

script_dir = os.path.dirname(os.path.abspath(__file__))

# Assemble an example data dict
fs = 2**8  # Hz
duration = 10  # s

amp1 = 5.0 # cts
freq1 = 15 # Hz

std = nu.convert_density_to_std(1.0, fs) # cts

number_of_samples = duration * fs # samples
times = np.linspace(0, duration, number_of_samples) # s

aa = std * np.random.randn(number_of_samples)
bb = amp1 * np.sin(2 * np.pi * freq1 * times)
cc = aa + bb

chan = "Noisy sine"

# Assemble data_dict
data_dict = {}
data_dict[chan] = {}
data_dict[chan]["data"] = cc
data_dict[chan]["fs"] = fs
data_dict[chan]["duration"] = duration

# Make deepcopy
daniell_data_dict = copy.deepcopy(data_dict)

# Decide the binwidth
binwidth = 0.5 # Hz
overlap = 0 # for easy comparison

# Normal Welch's method
averages = nu.dtt_averages(duration, binwidth, overlap)

data_dict = nu.calc_psd(data_dict, chan, averages, binwidth, overlap)

# Daniell method
daniell_data_dict = nu.calc_daniell_psd(daniell_data_dict, chan, binwidth)


###   Figure   ###
fig, s1 = plt.subplots(1)

s1.loglog(data_dict[chan]["ff"], data_dict[chan]["PSD"], alpha=0.7, label="Welch")
s1.loglog(daniell_data_dict[chan]["ff"], daniell_data_dict[chan]["PSD"], alpha=0.7, label="Daniell")

s1.set_title("Comparison of Welch and Daniell methods")
s1.set_ylabel("PSD [cts^2/Hz]")
s1.set_xlabel("Frequency [Hz]")

s1.grid()
s1.grid(which="minor", ls="--", alpha=0.3)
s1.legend()

filename = "compare_daniell_psd.pdf"
full_filename = os.path.join(script_dir, filename)
print(f"Saving figure to")
print(f"{full_filename}")
plt.tight_layout()
plt.savefig(full_filename)
