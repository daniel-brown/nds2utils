"""
Gets and plots minute trends of some ISI ST2 SENSCOR channels

Craig Cahillane
Nov 6, 2019
"""

import nds2utils as nu

buffers = nu.find_channels("H1:*ISI*ITMY*ST2*SENSCOR*CPS.mean", channel_type="MTREND")
channels = nu.make_array_from_buffers(buffers)
gps_stop = 1256361546
duration = 6000
gps_start = gps_stop - duration

host_server = "nds.ligo-wa.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

dataDict = nu.acquire_data(
    channels,
    gps_start,
    gps_stop,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

import matplotlib.pyplot as plt

nu.plot_raw_data(
    dataDict, seconds=duration
)  # can plot full duration, don't need to decimate minute trends
plt.show()
