"""
Compare median vs mean averaging at time of glitch in DARM

Craig Cahillane
November 6, 2019
"""

import nds2utils as nu

channels = ["H1:CAL-DELTAL_EXTERNAL_DQ"]
gps_start_glitch = 1257123335
duration = 60 * 20  # 20 minutes of data
gps_stop_glitch = gps_start_glitch + duration
binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

host_server = "nds.ligo.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

dataDict_mean = nu.get_psds(
    channels,
    gps_start_glitch,
    gps_stop_glitch,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)
dataDict_median = nu.get_psds(
    channels,
    gps_start_glitch,
    gps_stop_glitch,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
    averaging="median",
)

# Make the DARM calibration using zpk
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
units = "m"
dataDict_mean = nu.calibrate_chan(
    dataDict_mean,
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,
)
dataDict_median = nu.calibrate_chan(
    dataDict_median,
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,
)

# Define some plot parameters
xlims = [7, 7000]  # set the x-axis limits for the plot
ylims = [1e-20, 1e-15]  # set the y-axis limits for the plot

import matplotlib.pyplot as plt

nu.compare_asds(
    dataDict_mean,
    dataDict_median,
    label1="Mean",
    label2="Median",
    title="Mean and median averaging DARM spectra at time of glitch",
    logbin=True,
    xlims=xlims,
    ylims=ylims,
    units=units,
)
plt.show()
