"""
Plots the calibrated DARM ASDs for both Hanford and Livingston.

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu

channelsH1 = ["H1:CAL-DELTAL_EXTERNAL_DQ"]
gps_start = 1256771562
gps_stop = 1256771712
binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

host_server = "nds.ligo.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

data_dict_h1 = nu.get_psds(
    channelsH1,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

channelsL1 = ["L1:CAL-DELTAL_EXTERNAL_DQ"]
host_server = "nds.ligo.caltech.edu"
port_number = 31200
data_dict_l1 = nu.get_psds(
    channelsL1,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

# Make the DARM calibration using zpk
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
units = "m"
data_dict_h1 = nu.calibrate_chan(
    data_dict_h1,
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,
)
data_dict_l1 = nu.calibrate_chan(
    data_dict_l1,
    "L1:CAL-DELTAL_EXTERNAL_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,
)

# Define some plot parameters
xlims = [7, 7000]  # set the x-axis limits for the plot
ylims = [1e-20, 1e-15]  # set the y-axis limits for the plot

import matplotlib.pyplot as plt

plt.ion()  # don't pause to show the first plot
fig = nu.plot_asds(
    data_dict_l1,
    title="Calibrated and Logbinned DARM spectra",
    logbin=True,
    xlims=xlims,
    ylims=ylims,
    units=units,
)
plt.ioff()  # pause to show the second plot
nu.plot_asds(
    data_dict_h1,
    title="Calibrated and Logbinned DARM spectra",
    logbin=True,
    xlims=xlims,
    ylims=ylims,
    units=units,
    fig=fig,
)
plt.show()
